<?php

class SchemaValidatorTest extends \PHPUnit\Framework\TestCase
{
    public function testOkSchema()
    {
        $validator = new \BmPlatform\Support\SchemaValidator();

        $this->assertNull($validator(require(__DIR__.'/../vendor/bm-platform/abstraction/examples/schema.php')));
    }
}