<?php

class HttpClientTest extends \PHPUnit\Framework\TestCase
{
    public function testMakesRequests()
    {
        \Illuminate\Support\Facades\Event::expects('dispatch');

        $client = new \BmPlatform\Support\Http\HttpClient();
        $resp = $client->get('https://example.com');
        $this->assertEquals(200, $resp->getStatusCode());
    }
}