<?php

enum IntEnum: int {
    use \BmPlatform\Support\EnumExtension;

    case a = 1;
    case b = 2;
}

enum StringEnum: string {
    use \BmPlatform\Support\EnumExtension;

    case A = 'a';
    case B = 'b';
}

enum TestEnum {
    use \BmPlatform\Support\EnumExtension;

    case a;
    case b;
}

class EnumExtensionTest extends \PHPUnit\Framework\TestCase
{
    public function testIntCoersion()
    {
        $this->assertEquals(IntEnum::a, IntEnum::coerce(1));
        $this->assertEquals(IntEnum::a, IntEnum::coerce('a'));
        $this->assertTrue(IntEnum::a->is(1));
        $this->assertTrue(IntEnum::a->in([ IntEnum::a ]));
        $this->assertFalse(IntEnum::a->in([ IntEnum::b ]));
    }

    public function testStringCoersion()
    {
        $this->assertEquals(StringEnum::A, StringEnum::coerce('a'));
        $this->assertEquals(StringEnum::A, StringEnum::coerce('A'));
        $this->assertTrue(StringEnum::A->is(StringEnum::A));
    }

    public function nonBackedEnum()
    {
        $this->assertEquals(TestEnum::a, TestEnum::coerce('a'));
    }
}
