<?php

class ParamsMiddlewareTest extends \PHPUnit\Framework\TestCase
{
    public function testParamsConverted()
    {
        $middleware = new \BmPlatform\Support\Http\ParamsMiddleware(function (\Psr\Http\Message\RequestInterface $request, array $options) {
            return $request->getUri();
        });

        $request = new \GuzzleHttp\Psr7\Request('POST', \GuzzleHttp\Psr7\Utils::uriFor('https://example.com/:param1/:_param2'));

        $uri = $middleware($request, [
            'params' => [
                'param1' => 'p1',
                '_param2' => 'p2',
            ],
        ]);

        $this->assertEquals('https://example.com/p1/p2', (string)$uri);

        $request = new \GuzzleHttp\Psr7\Request('POST', \GuzzleHttp\Psr7\Utils::uriFor(':domain/:param1/:_param2'));

        $uri = $middleware($request, [
            'params' => [
                'domain' => 'https://example.com',
                'param1' => 'p1',
                '_param2' => 'p2',
            ],
        ]);

        $this->assertEquals('https://example.com/p1/p2', (string)$uri);
    }
}