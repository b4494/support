<?php

namespace BmPlatform\Support;

trait LocalizedEnum
{
    public static function toSelectArray(): array
    {
        return collect(static::cases())->mapWithKeys(fn (\BackedEnum|self $case) => [
            $case->value => $case->localizedName(),
        ])->all();
    }

    public function localizedName(): string
    {
        $key = 'enums.'.class_basename($this).'.'.str($this->name)->snake();

        return ($key === $value = trans($key)) ? $this->name : $value;
    }
}