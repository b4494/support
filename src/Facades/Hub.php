<?php

namespace BmPlatform\Support\Facades;

use BmPlatform\Abstraction\Interfaces\AppInstance;
use Illuminate\Support\Facades\Facade;

/**
* @method void registerAppType(string $type, callable $createHandler, array $options = [])
* @method \BmPlatform\Abstraction\Interfaces\AppHandler createHandler(AppInstance $appInstance)
* @method AppInstance integrate(\BmPlatform\Abstraction\DataTypes\AppIntegrationData $data)
* @method AppInstance|null findAppById(string $type, string $id)
* @method string getAuthToken(AppInstance $appInstance, \BmPlatform\Abstraction\DataTypes\Operator|string|null $operator = null)
* @method string webUrl(string $authToken)
* @method string getCallbackId(AppInstance $appInstance)
* @method AppInstance|null findAppByCallbackId(string $type, string $id)
 */
class Hub extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \BmPlatform\Abstraction\Interfaces\Hub::class;
    }
}