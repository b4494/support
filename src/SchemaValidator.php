<?php

namespace BmPlatform\Support;

use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\Validator;

class SchemaValidator
{
    public function __construct(protected ?\stdClass $reference = null)
    {
        //
    }

    /** @throws \InvalidArgumentException */
    public function __invoke(array $schema): void
    {
        if (!$this->reference) {
            $this->reference = $this->getDefaultSchema();
        }

        $validator = new Validator();

        $schemaObject = json_decode(json_encode($schema));

        $result = $validator->validate($schemaObject, $this->reference);

        if ($result->isValid()) return;

        foreach ((new ErrorFormatter())->format($result->error()) as $key => $fieldErrors) {
            foreach ($fieldErrors as $error) {
                $errors[] = sprintf("[%s] %s", $key, $error);
            }
        }


        throw new \InvalidArgumentException(implode(PHP_EOL, $errors));
    }

    /**
     * @return mixed
     */
    protected function getDefaultSchema(): mixed
    {
        static $schema;

        if (isset($schema)) return $schema;

        $paths = [
            __DIR__.'/../../abstraction/resources/schema.json',
            __DIR__.'/../vendor/bm-platform/abstraction/resources/schema.json',
        ];

        foreach ($paths as $path) {
            if (file_exists($path)) return $schema = json_decode(file_get_contents($path));
        }

        throw new \RuntimeException('Missing default schema. Please require bm-platform/abstraction package.');
    }
}