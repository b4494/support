<?php

namespace BmPlatform\Support;

use Illuminate\Support\Str;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberType;
use libphonenumber\PhoneNumberUtil;

class Helpers
{
    /**
     * @param array $items
     *
     * @return array
     */
    public static function withoutEmptyStrings(array $items): array
    {
        $items = array_map(function ($value) {
            if (is_array($value)) $value = self::withoutEmptyStrings($value);

            return $value === '' || $value === [] ? null : $value;
        }, $items);

        // Don't filter simple lists (otherwise it will be converted to object rather than array)
        return isset($items[0]) ? $items : array_filter($items, function ($item) {
            return $item !== '' && $item !== null && $item !== [];
        });
    }

    public static function extractPhoneNumber($value, $countryCode = null): ?string
    {
        $service = PhoneNumberUtil::getInstance();

        try {
            if (!$countryCode && !Str::contains($value, '+')) $value = '+'.$value;

            $number = $service->parse($value, $countryCode ? strtoupper($countryCode) : null);

            return match ($service->getNumberType($number)) {
                PhoneNumberType::PERSONAL_NUMBER, PhoneNumberType::MOBILE, PhoneNumberType::FIXED_LINE_OR_MOBILE =>
                    $number->getCountryCode()
                    .($number->hasNumberOfLeadingZeros() ? str_repeat('0', $number->getNumberOfLeadingZeros()) : '')
                    .$number->getNationalNumber(),
                default => null,
            };
        }

        catch (NumberParseException $e) {
            return null;
        }
    }
}