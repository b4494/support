<?php

namespace BmPlatform\Support;

trait EnumExtension
{
    public function in(array|self $cases): bool
    {
        return in_array($this, is_array($cases) ? $cases : func_get_args());
    }

    public function is($value): bool
    {
        return $this === static::coerce($value);
    }

    public function isNot($value): bool
    {
        return !$this->is($value);
    }

    public static function coerce($value): static
    {
        if ($value instanceof static) return $value;

        $caseValue = is_subclass_of(static::class, \BackedEnum::class)
            ? static::cases()[0]->value
            : null;

        return match (true) {
            is_numeric($value) && is_int($caseValue) => static::from($value),
            is_string($value) && is_string($caseValue) => static::tryFrom($value) ?: static::findCaseByName($value),
            !is_numeric($value) => static::findCaseByName($value),
            default => throw new \InvalidArgumentException('Failed to coerce enum'),
        };
    }

    public static function findCaseByName(string $name): static
    {
        if (!$case = collect(static::cases())->firstWhere(fn ($case) => $case->name == $name)) {
            $class = static::class;

            throw new \InvalidArgumentException("Invalid case $class::$name");
        }

        return $case;
    }

    public static function values(?array $cases = null): array
    {
        return collect($cases ?: static::cases())->map(fn (\BackedEnum $case) => $case->value)->all();
    }

    public static function names(?array $cases = null): array
    {
        return collect($cases ?: static::cases())->map(fn (\BackedEnum $case) => $case->name)->all();
    }
}