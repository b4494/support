<?php

namespace BmPlatform\Support\Events;

use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class HttpRequestFinished
{
    public function __construct(
        public readonly Carbon $timestamp,
        public readonly Carbon $startTime,
        public readonly RequestInterface $request,
        public readonly ?ResponseInterface $response,
        public readonly array $options,
        public readonly ?\Throwable $exception = null,
    ) {
        //
    }
}