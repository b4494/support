<?php

namespace BmPlatform\Support\Http;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * This one allows using parameters in URI like `https://example.com/:id/edit`.
 * Parameters are set using `params` request option.
 */
class ParamsMiddleware
{
    /** @var callable(RequestInterface, array): PromiseInterface */
    protected $next;

    public function __construct(callable $next)
    {
        $this->next = $next;
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function __invoke(RequestInterface $request, array $options)
    {
        $fn = $this->next;

        return $fn($request->withUri($this->replaceParams($request->getUri(), $options['params'] ?? [])), $options);
    }

    protected function replaceParams(UriInterface $uri, array $params)
    {
        return new Uri(preg_replace_callback('/:(?<param>[a-z_-][a-z0-9_-]*)/i', function ($matches) use ($params) {
            if (!isset($params[$matches['param']])) {
                throw new ErrorException(ErrorCode::BadRequest, "Param [{$matches['param']}] is missing.");
            }

            return $params[$matches['param']];
        }, (string)$uri));
    }
}