<?php

namespace BmPlatform\Support\Http;

use BmPlatform\Support\Events\HttpRequestFinished;
use Carbon\Carbon;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Support\Facades\Event;
use Psr\Http\Message\RequestInterface;

class EventGeneratorMiddleware
{
    /** @var callable(RequestInterface, array): PromiseInterface */
    protected $next;

    public function __construct(callable $next)
    {
        $this->next = $next;
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function __invoke(RequestInterface $request, array $options)
    {
        $fn = $this->next;
        $start = Carbon::now();

        return $fn($request, $options)->then(function ($response) use ($start, $request, $options) {
            Event::dispatch(new HttpRequestFinished(Carbon::now(), $start, $request, $response, $options));

            return $response;
        }, function ($e) use ($start, $request, $options) {
            Event::dispatch(new HttpRequestFinished(Carbon::now(), $start, $request, null, $options, $e));

            throw $e;
        });
    }
}