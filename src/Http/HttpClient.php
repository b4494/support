<?php

namespace BmPlatform\Support\Http;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Utils;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Psr\Http\Message\ResponseInterface;

/**
 * @method get(string $uri, array $options = []): mixed
 * @method post(string $uri, array $options = []): mixed
 * @method put(string $uri, array $options = []): mixed
 * @method patch(string $uri, array $options = []): mixed
 * @method delete(string $uri, array $options = []): mixed
 */
class HttpClient
{
    private Client $client;

    public function __construct(array $options = [])
    {
        $this->client = $this->createClient($options);
    }

    public function __call(string $name, array $arguments): mixed
    {
        return $this->request($name, ...$arguments);
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function request(string $method, string $path, array $options = []): mixed
    {
        try {
            return $this->processResponse($response = $this->client->request($method, $path, $options));
        }

        catch (ConnectException $e) {
            throw $this->handleConnectException($e);
        }

        finally {
            if (isset($response)) $response->getBody()->close();
        }
    }

    protected function processResponse(ResponseInterface $response): mixed
    {
        return $response;
    }

    public static function createHandlerStack(callable $handler = null): HandlerStack
    {
        return tap(new HandlerStack($handler ?: Utils::chooseHandler()), function (HandlerStack $stack) {
            $stack->push(static::eventGeneratorMiddleware());
            $stack->push(static::paramsMiddleware());
            $stack->push(Middleware::prepareBody());
        });
    }

    protected function createClient(array $options)
    {
        if (!isset($options['handler'])) {
            $options['handler'] = self::createHandlerStack();
        }

        return new Client(array_merge($this->defaultConfig(), $options));
    }

    protected static function paramsMiddleware(): callable
    {
        return static function (callable $next): ParamsMiddleware {
            return new ParamsMiddleware($next);
        };
    }

    protected static function eventGeneratorMiddleware(): callable
    {
        return static function (callable $next): EventGeneratorMiddleware {
            return new EventGeneratorMiddleware($next);
        };
    }

    protected function defaultConfig(): array
    {
        return [
            RequestOptions::CONNECT_TIMEOUT => 1.0,
            RequestOptions::TIMEOUT => 10.0,

            // Verification will be enabled only in production
            RequestOptions::VERIFY => (($config = Config::getFacadeRoot())) && !$config->get('app.debug', false),
        ];
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    protected function handleConnectException(ConnectException $e): ErrorException
    {
        $ctx = $e->getHandlerContext();

        if (($ctx['errno'] ?? null) == CURLE_OPERATION_TIMEOUTED) {
            return new ErrorException(ErrorCode::OperationTimedOut, $e->getMessage(), $e);
        } else {
            return new ErrorException(ErrorCode::ConnectionFailed, $e->getMessage());
        }
    }
}